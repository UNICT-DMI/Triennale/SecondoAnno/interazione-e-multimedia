void setup()
{
  size(500,500);
  background(0);
}

void draw()
{
  noFill();
  stroke(255);
  beginShape();
  vertex(50,50);
  vertex(150,150);
  vertex(50,150);
  endShape(CLOSE);
}