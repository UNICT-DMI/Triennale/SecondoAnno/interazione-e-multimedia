PImage image;
PImage imGray;
PImage bitplane;
boolean saved;

//Setup
void setup()
{
  //Finestra per contenere un'immagine di dimensioni 256x256
  size(768,256);
  image=loadImage("lena.png");
  image.resize(image.width/2,image.height/2);
  saved=false;
  
  image(image,0,0);
 
  imGray=imToGray(image);
  image(imGray,imGray.width,0);

  bitplane=estraiPianoN(imGray,8);
  image(bitplane,bitplane.width*2,0);
}


void draw()
{
}


//**************Funzioni**************//


//Gestione KeyPressed
void keyPressed()
{
  int k=key;
  if ((k>'0')&&(k<'9'))
  {
    bitplane=estraiPianoN(imGray,k-'0');
    image(bitplane,image.width*2,0);   
  }
  
  if (((k=='s')||(k=='S'))&&(!saved))
  {
    saved=true;
    salvaBitPlane(imGray);
  }
  
}

//Converte l'immagine in una versione a scala di grigi
PImage imToGray(PImage I)
{
  PImage gray=I.copy();
  
  gray.loadPixels();
  I.loadPixels();
  
  float R,G,B;
  for(int i=0;i<gray.pixels.length;i++)
  {
    R=red(gray.pixels[i]);
    G=green(gray.pixels[i]);
    B=blue(gray.pixels[i]);
    
    gray.pixels[i]=color(0.5*R+0.2*G+0.3*B);
  }
  
  gray.updatePixels();
  return gray;
}

//Estrai bitplane n-esimo
PImage estraiPianoN(PImage I, int n)
{
  PImage plane=createImage(I.width,I.height,RGB);
  
  plane.loadPixels();
  I.loadPixels();
  
  int B;
  for(int i=0;i<I.pixels.length;i++)
  {
   
    B=(int(green(I.pixels[i])))>>(n-1);
    B=B&1;
    
    //0 e 1 sono trasformati in 0 e 255 rispettivamente per la visualizzazione in bianco e nero
    plane.pixels[i]=color(B*255);
  }
  
  plane.updatePixels();
  return plane;
}

//Salva gli 8 bitplane dell'immagine
void salvaBitPlane(PImage I)
{
 PImage B;
 for(int i=1; i<=8; i++)
 {
   B=estraiPianoN(I,i);
   B.save(savePath("b"+i+".png"));
 }
}