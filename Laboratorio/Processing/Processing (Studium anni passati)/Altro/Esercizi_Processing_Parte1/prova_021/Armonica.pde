class Armonica
{
  float frequenza;
  float ampiezza;
  float fase;
  
  //Costruttore 
  Armonica(float f, float a, float p)
  {
    frequenza=f;
    ampiezza=a;
    fase=p;
  }
  
  //Funzione che campiona ad intervalli equidistanti e restituisce l'array di float
  //In questa implementazione viene sempre ricalcolato quando ci serve. Si potrebbe pure calcolare e salvare in un attributo float[]
  float[] generaPlot()
  {
    float[] plot=new float[100];
    
    for (int i=0;i<100;i++)
    {
      //Campioniamo la funzione ad intervalli fissi di 1/100
      float t=float(i)/100;
      
      plot[i]=ampiezza*sin(2*PI*frequenza*t + fase);
    }
    
    return plot;
  }
}