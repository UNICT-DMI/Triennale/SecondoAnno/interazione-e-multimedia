PImage image;
float[] H1;
void setup() 
{
  background(255);
  size(800, 512);
  image = loadImage("lena.png");
  image.filter(GRAY);
  image(image,0,0);
  
  H1=calcolaIstogramma(image);
  stampaIstrogramma(H1,525,50);
  
}

void draw() 
{


}


//Visualizza Istogramma
void stampaIstrogramma(float[] H, float posX, float posY)
{
 
  noFill();
  stroke(128);
  rect(posX,posY,256,100);
  
  stroke(0);
  
  float K=max(H);
  for(int i=0; i<256;i++)
  {
    //line(posX+i,posY+100,posX+i,(posY+100)-(100)*(H[i]/K));
    line(posX+i,posY+100,posX+i,(posY+100)-lerp(0,100,H[i]/K));
  }
}

//Calcola Istrogramma
float[] calcolaIstogramma(PImage I)
{
  float[] hist=new float[256];
  
  I.loadPixels();
  
  for(int i=0; i<I.pixels.length;i++)
  {
    hist[int(green(I.pixels[i]))]++;
  }
  
  //Normalizziamo (somma 1)
  for(int i=0;i<256;i++)
  {
    hist[i]/=I.pixels.length;
  }
  
  return hist;
}



int pos(int i, int j, int w)
{
  return i*w+j;
}