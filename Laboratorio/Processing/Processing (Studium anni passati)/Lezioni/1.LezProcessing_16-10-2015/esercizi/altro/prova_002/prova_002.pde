//ellipse; modes; stroke; fill; smooth();

final int X=500;
final int Y=300;


void setup()
{
  size(X,Y);
  background(0);
  
  //ellipseMode(CENTER); //Default
  
  //ellipseMode(RADIUS);
  //ellipseMode(CORNER);
  //ellipseMode(CORNERS);

  //smooth();
}

void draw()
{
  noStroke();
  stroke(0,0,255);
  strokeWeight(5);
  fill(255,0,0);
  
  ellipse(100,100,150,150);
  
  stroke(0,255,255);
  strokeWeight(2);
  fill(255,0,255);
  
  rect(200,200,50,50);
  
}
