//Studiamo il funzionamento di setup() e draw().
//Proviamo a modificare il background n volte al secondo cambiando il framerate
boolean white;

void setup()
{
  size(500,500);
  background(#000000);
  white=false;
  frameRate(1);
  //println(frameRate);  //Spieghiamo più avanti il significato di questo valore 
}

void draw()
{
  if (white)
  {
    background(0);
    white=false;
  }
  else
  {
    background(255);
    white=true;
  } 
  
}
