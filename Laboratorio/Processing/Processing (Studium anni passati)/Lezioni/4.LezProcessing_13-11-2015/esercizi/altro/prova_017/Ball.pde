public class Ball
{
  //Membri o attributi della classe
  private float posX;
  private float posY;
  private float speedX;
  private float speedY;
  
  //Costruttore
  
  public Ball(float posX, float posY, float speedX, float speedY)
  {
   this.posX=posX;
   this.posY=posY;
   this.speedX=speedX;
   this.speedY=speedY;
  }
  
  public Ball(float posX, float posY)
  {
    this(posX,posY,0,0);
  }
  
  ///
  
  public void run()
  {
    move();
    bounce();
    gravity();
    display();
  }
  
  public void display()
  {
    noStroke();
    fill(255,255,255);
    ellipse(posX,posY,50,50);
  }
  
  public void move()
  {
    posX=posX+speedX;
    posY=posY+speedY;
  }
  
  public void bounce()
  {
    if ((posX > width-25) || (posX<25))
    {
      speedX*=-1;
    }
    
    if (posY > height-25) 
    {
      speedY*=-1;
      posY=height-25;
    }
    
    if (posY<25)
    {
      speedY*=-1;
      posY=25;
    }
    
  }
  
  public void gravity()
  {
    speedY+=0.3;
  }
  
  
}