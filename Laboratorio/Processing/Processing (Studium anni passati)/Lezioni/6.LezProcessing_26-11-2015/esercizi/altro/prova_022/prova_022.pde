PImage image;

void setup() {
  size(512, 512);
  image = loadImage("../lena.png");
}

void draw() {
  image(image, 0, 0);
}