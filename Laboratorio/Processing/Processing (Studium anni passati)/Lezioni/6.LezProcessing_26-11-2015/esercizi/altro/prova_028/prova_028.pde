PImage image;

void setup() 
{
  size(500, 500);
  image = createImage(250,250,RGB);
  
  
  for(int i=0; i<image.width;i++)
  {
    for (int j=0; j<image.height;j++)
    {
      //color c=color(int(random(0,255)),int(random(0,255)),int(random(0,255)));
      
      color c= color(lerp(0,255,(float(i))/image.width),lerp(0,255,(float(j))/image.height),lerp(0,255,(float(i*j))/(image.height*image.width)));
      image.set(i,j,c);
    }
  }
  fill(0);
}

void draw() 
{
  background(255);
  image(image,0,0);
  noStroke();
  rect(image.width,0,image.width,image.height);
  checkMouse();
}

void checkMouse()
{
  if((mousePressed)&&(mouseButton==LEFT))
  {
    int posX=mouseX;
    int posY=mouseY;
    
    if((posX>0) && (posX<image.width) && (posY>0) && (posY<image.height))
    {
      color c= image.get(posX,posY);
      fill(c);
    }
  }
  
}