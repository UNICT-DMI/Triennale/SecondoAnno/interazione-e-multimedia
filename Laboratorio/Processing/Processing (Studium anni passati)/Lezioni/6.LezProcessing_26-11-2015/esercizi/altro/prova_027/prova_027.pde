PImage image;
PImage imageT;


void setup() {
  size(1000, 700);
  image = createImage(250,250,RGB);
  
  //Carico i pixel nell'attributo pixels
  image.loadPixels();
  
  
  for (int i = 0; i < image.pixels.length/2; i++) 
  {
    image.pixels[i] = color(0,255,255); 
  }
  image.updatePixels();
  
  color brown= color(192,128,64);
  
  for (int i = image.pixels.length/2; i < image.pixels.length; i++) 
  {
    image.pixels[i] = brown; 
  }
  image.updatePixels();
  
  println("Larghezza:" + image.width);
  println("Altezza:" + image.width);

///////////
  imageT = createImage(250,250,ARGB);
  int dim=imageT.pixels.length;
  for (int i = 0; i < dim; i++) 
  {
    imageT.pixels[i] = color(255,0,0,lerp(0,255,(float(i))/dim)); 
  }
  imageT.updatePixels();
////

}

void draw() {
  background(255);
  
  image(image,0,0);
  image(imageT,0,300);
  
}