PImage img;

void setup()
{
 size(512,512);
 img=loadImage("lena.png");
 
 color c=img.get(int(img.width/2),int(img.height/2));
 
 img.loadPixels();
 
 //indice per accedere alla posizione (i,j)= i*width+j
 color c1=img.pixels[256+256*img.width];
 
 println(red(c));
 println(green(c));
 println(blue(c));
 
 println(red(c1));
 println(green(c1));
 println(blue(c1));
 noStroke();
 
}

void draw()
{
  image(img,0,0,img.width/2,img.height/2);
  
  
  if(mousePressed)
  {
    color c=img.get(mouseX,mouseY);
    fill(c);
  }
  rect(img.width/2,0,img.width/2,img.height/2);
}