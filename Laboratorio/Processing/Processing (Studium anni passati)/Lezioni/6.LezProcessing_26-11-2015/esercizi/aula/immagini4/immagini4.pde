PImage img;
PImage R;
PImage G;
PImage B;

void setup()
{
  size(512,512);
  img =loadImage("lena.png");
  
  
  img.resize(img.width/2,img.height/2);
  
  R =createImage(img.width,img.height,RGB);
  G =createImage(img.width,img.height,RGB);
  B =createImage(img.width,img.height,RGB);
  
  image(img,0,0);
  img.loadPixels();
  R.loadPixels();
  G.loadPixels();
  B.loadPixels();
  
  for(int i=0; i<img.pixels.length;i++)
  {
    R.pixels[i]=color(red(img.pixels[i]),0,0);
    G.pixels[i]=color(0,green(img.pixels[i]),0);
    B.pixels[i]=color(0,0,blue(img.pixels[i]));
  }
  
  image(R,img.width,0);
  image(G,0,img.height);
  image(B,img.width,img.height);
  
  img.filter(INVERT);
  imageMode(CENTER);
  image(img,width/2,height/2);
  
  img.save(savePath("lena2.jpg"));
  
}

void draw()
{
 
}