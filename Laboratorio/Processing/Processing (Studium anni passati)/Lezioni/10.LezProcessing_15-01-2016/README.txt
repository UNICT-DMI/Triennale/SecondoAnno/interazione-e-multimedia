In questa lezione abbiamo implementato delle funzioni applicare i filtri di Minimo e Mediano. Dopodich� siamo passati alla convoluzione applicandola con diversi filtri.

Una particolare enfasi � stata messa sull'importanza dell' output di un'operazione di convoluzione. In generale il risultato NON � infatti un'immagine, ma un segnale generico.

Bisogna quindi distinguere i vari casi. I filtri di media danno come output un'immagine, mentre i filtri di edge detection NO! Il risultato dell'applicazione di questi ultimi pu� in ogni caso essere rappresentato come immagine (dopo un'opportuna normalizzazione), ma al solo scopo di VISUALIZZAZIONE.

Altra nota importante riguarda il metodo di accesso alla matrice dei pixel. Nella classe PImage (e in generale nelle immagini digitali), la prima coordinata indica la colonna e la seconda la riga (Es: I.get(x,y)). Nelle matrici classiche (Es: float[][]), il primo indice indica invece la riga e il secondo la colonna.
Per cui, la relazione tra gli elementi di PImage e quelli di una comune matrice sar� sempre I.get(i,j) uguale a I[j][i].Nel codice si noter� spesso questa inversione.