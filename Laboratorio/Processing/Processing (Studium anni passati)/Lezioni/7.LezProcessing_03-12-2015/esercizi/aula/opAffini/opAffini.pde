PImage image;
PImage S;
PImage R;
PImage SH;

float th=PI/4;
float cx=2;
float cy=2;
float sh=0.2;

void setup() 
{ 
  size(512, 512);
  background(0);
  image=loadImage("lena.png");
  image.resize(image.width/2,image.height/2); 
  
  S=createImage(image.width,image.height,RGB);
  R=createImage(image.width,image.height,RGB);
  SH=createImage(image.width,image.height,RGB);
  
  int dimW=image.width;
  int dimH=image.height;
  
  int x;
  int y;
  
  image.loadPixels();
  S.loadPixels();
  R.loadPixels();
  SH.loadPixels();
  
  for(int v=0;v<dimW;v++)
  {
    for(int w=0;w<dimH;w++)
    {
      int v1=v-dimW/2;
      int w1=w-dimH/2;
      
      x=round(v1*cx);
      y=round(w1*cy);

      x=x+dimW/2;
      y=y+dimH/2;
      
      
      if ((x>=0)&&(x<dimW)&&(y>=0)&&(y<dimH))
      {
        S.pixels[pos(x,y,dimW)]=image.pixels[pos(v,w,dimW)];
      }
      
      
      x=round(v1*cos(th)-w1*sin(th));
      y=round(v1*sin(th)+w1*cos(th));

      x=x+dimW/2;
      y=y+dimH/2;
      
      if ((x>=0)&&(x<dimW)&&(y>=0)&&(y<dimH))
      {
        R.pixels[pos(x,y,dimW)]=image.pixels[pos(v,w,dimW)];
      }
      
      x=round(v1);
      y=round(v1*sh+w1);

      x=x+dimW/2;
      y=y+dimH/2;
      
      if ((x>=0)&&(x<dimW)&&(y>=0)&&(y<dimH))
      {
        SH.pixels[pos(x,y,dimW)]=image.pixels[pos(v,w,dimW)];
      }
      
    }
  }
  
  S.updatePixels();
  R.updatePixels();
  SH.updatePixels();
  
  image(image,0,0);
  image(S,dimW,0);
  image(R,0,dimH);
  image(SH,dimW,dimH);
 
}

void draw() 
{

}

int pos(int i,int j, int w)
{
  return w*i+j;
}