Robot[] robots;
int N=100;
char[] states={'R','G','B'};
int count;

void setup()
{
  size(500,500);
  background(0);
  robots=new Robot[N];
  char S;
  for (int i=0; i<N;i++)
  {
    S=states[(int)(random(0,3))];
    robots[i]=new Robot(random(0,width), random(0,height),S);
  }
  count=1;
}

void draw()
{
  for (int i=0;i<N;i++)
  {
    if (count==100)
       robots[i].cambiaStato();
    robots[i].run();
  }
  
  if (count==100)
    count=0;
  count++;
}

void keyPressed()
{
  if (key=='r')
  {
    setup();
  }
}