
class Alieno
{
  float posX;
  float posY;
  float energia;
  float dim;
  
  float speedX=0;
  float speedY=0;
  boolean vivo=true;
  boolean arrivato=false;
  
  Alieno(float posX, float posY, float energia, float dim)
  {
    this.posX=posX;
    this.posY=posY;
    this.energia=energia;
    this.dim=dim;
  }
  
  boolean tastoPremuto()
  {
    return ( (keyPressed) && ((key=='n') || (key=='N')) );
  }
  
  
  void scegliDirezione(Pozzo[] pozzi)
  {
    Pozzo vicino;
    float min;
    float d;
    
    min=dist(posX,posY,pozzi[0].posX,pozzi[0].posY);
    vicino=pozzi[0];
    
    for(int i=1;i<pozzi.length;i++)
    {
      d=dist(posX,posY,pozzi[i].posX,pozzi[i].posY);
      
      if (d<min)
      {  
        min=d;
        vicino=pozzi[i];
      }
    }
    
    float dx=posX-vicino.posX;
    float dy=posY-vicino.posY;
    
    speedX=-(dx/min)*3;
    speedY=-(dy/min)*3;
  }
  
  void muovi()
  {
      posX+=speedX;
      posY+=speedY;
      energia=energia-0.01;
  }
  
  
  void display()
  {
    
    rectMode(CENTER);
    noStroke();
    fill(0, vivo?255:0 ,0);
    rect(posX,posY,dim,dim);
  }
  
  void controllaArrivo()
  {
    float d;
    for(int i=0;i<pozzi.length;i++)
    {
      d=dist(posX,posY,pozzi[i].posX,pozzi[i].posY);
      
      if (d<=15)
      {  
       arrivato=true;
      }
    } 
  }
  
  void controllaMorte()
  {
    vivo = (energia>0);
  }
  
  
  void run(Pozzo[] pozzi)
  {
    
    if((tastoPremuto()) && (vivo) && (!arrivato))
    {
      scegliDirezione(pozzi);
      muovi();
    }
    
    controllaArrivo();
    controllaMorte();
    
    if (!arrivato)
      display();
    
    
  }
  
}