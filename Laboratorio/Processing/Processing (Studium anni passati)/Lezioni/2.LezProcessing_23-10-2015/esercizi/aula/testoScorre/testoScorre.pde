//text;
String s="Interazione & Multimedia";
int xPos=0;
int yPos=40;
int speed=3;
int dim=20;
boolean pause=false;
void setup()
{
  size(500,500);
  background(255);
  textAlign(CENTER);
  textSize(dim);
  fill(0);
  text(s,xPos,yPos);
  
}

void draw()
{
  background(255);
  xPos=xPos+speed;
  
  if (xPos>width+20)
  {
    xPos=0-(s.length()*dim);
    yPos=yPos+30;
  }
  
  if (yPos>height+40)
  {
    yPos=40;
  }
  text(s,xPos,yPos);
 
}

void keyPressed()
{
  if(key=='p')
  {
    if(!pause)
    {  
      noLoop();
      pause=true;
      text("Pause",width/2,height/2);
      
    }
    else
    {
      loop();
      pause=false;
    }
  } 
}
