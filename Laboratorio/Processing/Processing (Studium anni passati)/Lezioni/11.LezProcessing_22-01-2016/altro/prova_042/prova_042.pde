PImage image;
float[][][] imFT;
PImage imSpect;

void setup()
{
  size(512, 512);
  image=loadImage("lena.png");
  image.resize(image.width/2,image.height/2);
  image.filter(GRAY);

  image(image,0,0);
  
  println("Calcolo FT...");
  imFT=FourierT(image);
  println("...Calcolata");
  
  image(showSpectrum(imFT),0,image.height);
  image(showPhase(imFT),image.width,image.height);
}

PImage showPhase(float[][][] FT)
{
  int M=FT[0].length;
  int N=FT[0][0].length;
  float[][] Phase=new float[M][N];
  
  //Calcolo l'angolo di fase
  for(int i=0; i<M;i++)
  {
    for(int j=0;j<N;j++)
    {
      Phase[i][j]=atan2(FT[1][i][j],FT[0][i][j]);
    }
  }
  
  //Normalizzo la fase
  //
  float MinP=-PI;
  float MaxP=PI;
  
  for(int i=0; i<M;i++)
  {
    for(int j=0;j<N;j++)
    {
      Phase[i][j]=255*(Phase[i][j]- MinP)/(MaxP-MinP);
    }
  }
  
  //Converto in immagine
  PImage P=createImage(N,M,RGB);
  P.loadPixels();
  
  for(int i=0; i<M;i++)
  {
    for(int j=0;j<N;j++)
    {
      P.pixels[pos(i,j,P.width)]=color(Phase[j][i]);
    }
  }

  return P;
  
}

PImage showSpectrum(float[][][] FT)
{
  int M=FT[0].length;
  int N=FT[0][0].length;
  float[][] Spectrum=new float[M][N];
  
  //Calcolo lo spettro e il suo massimo per la normalizzazione successiva
  float MaxS=0;
  for(int i=0; i<M;i++)
  {
    for(int j=0;j<N;j++)
    {
      Spectrum[i][j]=sqrt(pow(FT[0][i][j],2)+pow(FT[1][i][j],2));
      if (Spectrum[i][j]>MaxS)
         MaxS=Spectrum[i][j];
    }
  }
  
  //Normalizzo lo spettro
  float c= 255/log(1+MaxS);
  
  for(int i=0; i<M;i++)
  {
    for(int j=0;j<N;j++)
    {
      Spectrum[i][j]=c*log(1+Spectrum[i][j]);
    }
  }
  
  //Converto in immagine
  PImage SP=createImage(N,M,RGB);
  SP.loadPixels();
  
  for(int i=0; i<M;i++)
  {
    for(int j=0;j<N;j++)
    {
      SP.pixels[pos(i,j,SP.width)]=color(Spectrum[j][i]);
    }
  }
  
  PImage TR= SP.get(0,0,SP.width/2,SP.height/2);
  PImage TL= SP.get(SP.width/2,0,SP.width/2,SP.height/2);
  PImage BR= SP.get(0,SP.height/2,SP.width/2,SP.height/2);
  PImage BL= SP.get(SP.width/2,SP.height/2,SP.width/2,SP.height/2);
  
  SP.set(SP.width/2,SP.width/2,TR);
  SP.set(0,0,BL);
  SP.set(SP.width/2,0,BR);
  SP.set(0,SP.height/2,TL);
  
  return SP;
  
}

float[][][] FourierT(PImage I)
{
  
  //Ricordiamo che le coordinate sono invertite
  float[][] FR=new float[I.height][I.width];
  
  I.loadPixels();
  
  //Converto l'immagine in float (per semplicità)
  //Ricordiamo che le coordinate sono invertite
  float[][] IF=new float[I.height][I.width];
  
  for(int i=0;i<I.width;i++)
  {
    for(int j=0;j<I.height;j++)
    {
      IF[j][i]=green(I.pixels[pos(i,j,I.width)]); 
    }
  }
  
  int M=IF.length;
  int N=IF[0].length;
  
  //Calcolo trasformata (parte reale e parte immaginaria)
  
  float[][] FTR=new float[M][N];
  float[][] FTI=new float[M][N];
  
  float[][] GR=new float[M][N];
  float[][] GI=new float[M][N];
  
  
  for(int x=0;x<M;x++)
  {
    for(int v=0;v<N;v++)
    {
      GR[x][v]=GXVReal(IF,x,v);
      GI[x][v]=GXVImag(IF,x,v);
    }
  }
  
   for(int u=0;u<M;u++)
  {
    for(int v=0;v<N;v++)
    {
      FTR[u][v]=FUVReal(GR,GI,u,v);
      FTI[u][v]=FUVImag(GR,GI,u,v);
    }
  }
  
  float[][][] FTrans=new float[2][M][N];
  FTrans[0]=FTR;
  FTrans[1]=FTI;
  
  return FTrans; 
}

float GXVReal(float[][] IF, int x, int v)
{
  int N=IF[0].length;
  
  float sum=0;
  for(int y=0; y<N; y++)
  {
    sum+=IF[x][y]*cos(2*PI*v*y/float(N));
  }
  return sum;
}

float GXVImag(float[][] IF, int x, int v)
{
  int N=IF[0].length;
  
  float sum=0;
  for(int y=0; y<N; y++)
  {
    sum+=IF[x][y]*(-1)*sin(2*PI*v*y/float(N));
  }
  return sum;
}

float FUVReal(float[][] GR, float[][] GI,int u,int v)
{
  int M=GR.length;
  
  float sum=0;
  float z=0;
  
  for(int x=0;x<M;x++)
  { 
     z=2*PI*u*x/float(M);
     sum+=GR[x][v]*cos(z)+GI[x][v]*sin(z);  
  }
  
  return sum;
}

float FUVImag(float[][] GR, float[][] GI,int u,int v)
{
  int M=GI.length;
  
  float sum = 0;
  float z = 0;
  
  for(int x=0;x<M;x++)
  { 
     z=2*PI*u*x/float(M);
     sum+=GI[x][v]*cos(z)-GR[x][v]*sin(z);  
  }
  return sum;
}


int pos(int i,int j,int w)
{
  return i+j*w;
}