PImage image;
float[][][] imFT;

void setup()
{
  size(512, 512);
  image=loadImage("lena.png");
  image.resize(image.width/4,image.height/4);
  image.filter(GRAY);

  image(image,0,0);
  
  println("Calcolo FT");
  imFT=FourierT(image);
  println("Calcolata");
  
  println(imFT[0][61][4]);

  
}

float[][][] FourierT(PImage I)
{
  
  //Ricordiamo che le coordinate sono invertite
  float[][] FR=new float[I.height][I.width];
  
  I.loadPixels();
  
  //Converto l'immagine in float (per semplicità)
  //Ricordiamo che le coordinate sono invertite
  float[][] IF=new float[I.height][I.width];
  
  for(int i=0;i<I.width;i++)
  {
    for(int j=0;j<I.height;j++)
    {
      IF[j][i]=green(I.pixels[pos(i,j,I.width)]); 
    }
  }
  
  int M=IF.length;
  int N=IF[0].length;
  
  //Calcolo trasformata (parte reale e parte immaginaria)
  float[][] FTR=new float[M][N];
  float[][] FTI=new float[M][N];
  for(int u=0;u<M;u++)
  {
    for(int v=0;v<N;v++)
    {
      FTR[u][v]=FUVReal(IF,u,v);
      FTI[u][v]=FUVImag(IF,u,v);
    }
  }
  
  float[][][] FTrans=new float[2][M][N];
  FTrans[0]=FTR;
  FTrans[1]=FTI;
  
  return FTrans; 
}

float FUVReal(float[][] IF,int u,int v)
{
  int M=IF.length;
  int N=IF[0].length;
  
  float c=1/float(M*N);
  float sum=0;
  for(int x=0;x<M;x++)
  {
    for(int y=0;y<N;y++)
    {
     sum+=IF[x][y]*cos(2*PI*(float(u*x)/M+float(y*v)/N)); 
    }
  }
  return sum;
}

float FUVImag(float[][] IF,int u,int v)
{
  int M=IF.length;
  int N=IF[0].length;
  
  float c=1/float(M*N);
  float sum=0;
  for(int x=0;x<M;x++)
  {
    for(int y=0;y<N;y++)
    {
     sum+=IF[x][y]*(-1)*sin(2*PI*(float(u*x)/M+float(y*v)/N)); 
    }
  }
  return sum;
}

int pos(int i,int j,int w)
{
  return i+j*w;
}