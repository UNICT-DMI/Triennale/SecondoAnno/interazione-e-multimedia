//Estrazione bitPlane
PImage image;
PImage bitPlane;

int k;
void setup()
{
  size(512, 256);
  image=loadImage("lena.png");
  image.resize(image.width/2,image.height/2);
  image.filter(GRAY);
  
  k=1;
  image(image,0,0);
  
  bitPlane=estraiBit(image,k);
  image(bitPlane,image.width,0);
  
}

void draw()
{
  image(image,0,0);
  image(bitPlane,image.width,0);
  textSize(15);
  text("Piano: " + k,0,15);
}

void keyPressed()
{
  if ((key=='+')&&(k<8))
  {  
    k++;
    bitPlane=estraiBit(image,k);
  } 
  if ((key=='-')&&(k>1))
  {  
    k--;
    bitPlane=estraiBit(image,k);
  }
}

PImage estraiBit(PImage I, int nb)
{
  PImage N= createImage(I.width,I.height,RGB);
  
  I.loadPixels();
  N.loadPixels();
  
  int c;
  
  for (int i=0; i<I.pixels.length;i++)
  {
    c=int(green(I.pixels[i]));
    //Shift a destra di nb-1 bit Si può ottenere dividendo per 2^(nb-1).
    c=c>>(nb-1);
    
    //Dopo lo shift, il bit meno significativo è quello che ci interessa.
    //Per estrarlo facciamo l'AND con un maschera 00...001 (valore 1). In alternativa si può usare l'operatore modulo 2.
    c=c&1;
    
    //Il risultato sarebbe una matrice a valori booleani (1 e 0), ma moltiplichiamo per 255 così da poterla visualizzare.
    N.pixels[i]=color(c*255);
  }
  
  N.updatePixels();
  
  return N;
 
}