class GreenBall extends Ball
{
  GreenBall(float posX, float posY, float speedX, float speedY)
  {
    super(posX,posY,speedX,speedY);
  }
  
  void display()
  {
    stroke(255,255,0);
    strokeWeight(5);
    fill(0,255,0);
    ellipse(super.posX,super.posY,50,50);
  }
}