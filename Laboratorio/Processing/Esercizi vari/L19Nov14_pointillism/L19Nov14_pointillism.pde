PImage p; 
int r,g,b; 
int rc, gc, bc; 

void setup () 
{
  
  p=loadImage("Bird 3.jpg"); 
  size(498, 296); 
  tint(0);                               //direttiva, colora tutta l'immagine di nero
  
  image(p,0,0); 
  frameRate(50); 
}

void draw() 
{                   
  color colore=get(mouseX, mouseY);     //assegna a colore il valore del pixel (x,y) in cui si trova il cursore
  r=int(red(colore));                   //trasforma in intero il valore della gradazione di rosso/verde/blu di colore 
  g=int(green(colore));                 //e lo assegna ad una variabile (r, g, b)
  b=int(blue(colore));
  
  color colore2=p.get(mouseX,mouseY);  
  rc=int(red(colore2));
  gc=int(green(colore2));
  bc=int(blue(colore2));
  
  fill(colore2); 
  noStroke(); 
  ellipse(mouseX, mouseY, 50, 50); 
  
  println("Valore pixel visualizzato ("+r+","+g+","+b+");"); 
  println("Valore pixel originale ("+rc+","+gc+","+bc+");");
}