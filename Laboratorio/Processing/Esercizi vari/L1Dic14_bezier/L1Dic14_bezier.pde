//la curva di Bezier ci permette di disegnare una curva avendo quattro punti definiti: due indicano le direzioni che 
//l'arco dovrà avere, i due centrali invece sottendono proprio l'arco.

int diametro=700; 
int xc1=10, yc1=diametro/2;             //coordinate secondo punto
int x1=diametro/6, y1=diametro/2;       //coordinate primo punto
int x2=5*diametro/6, y2=diametro/2;     //coordinate ultimo punto
int xc2=diametro-10, yc2=diametro/2;    //coordinate terzo punto

void setup() 
{
  size(diametro, diametro); 
  background(0); 
  noFill(); 
  ellipseMode(CENTER); 
}

void draw() 
{
  strokeWeight(3); 
  background(0); 
  if ((mousePressed)&&(dist(pmouseX, pmouseY,x1,y1)<4))
    {
      x1=mouseX; 
      y1=mouseY; 
    }
    
  if ((mousePressed)&&(dist(pmouseX, pmouseY,x2,y2)<4))
    {
      x2=mouseX; 
      y2=mouseY; 
    }
    
  if ((mousePressed)&&(dist(pmouseX, pmouseY,xc1,yc1)<4))
    {
      xc1=mouseX; 
      yc1=mouseY; 
    }
    
  if ((mousePressed)&&(dist(pmouseX, pmouseY,xc2,yc2)<4))
    {
      xc2=mouseX; 
      yc2=mouseY; 
    }
    
//disegnamo la curva
stroke(255); 
bezier(x1,y1,xc1,yc1,xc2,yc2,x2,y2); 

//disegno i punti
stroke(255,0,0); 
ellipse(xc1,yc1,3,3); 
stroke(0,0,255); 
ellipse(xc2,yc2,3,3); 
stroke(255,255,0);
ellipse(x1,y1,3,3);
stroke(0,255,0); 
ellipse(x2,y2,3,3);

//disegno le tangenti
stroke(140);
strokeWeight(1);
line(xc1,yc1,x1,y1);
line(x2,y2,xc2,yc2); 
}

