PrintWriter output; 

void setup() {
  //crea un nuovo file
  output=createWriter("positions.txt"); 
}

void draw() {
  point(mouseX, mouseY); 
  output.println(mouseX + "-" + mouseY);     // scrive le coord. sul file
 
}

void keyPressed() {
  output.flush();                            //scrive i dati rimanenti
  output.close();                            //finisce il file
  exit();                                    //stoppa il programma 
}

